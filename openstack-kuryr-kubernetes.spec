# {!?upstream_version: %global upstream_version %{version}%{?milestone}}
%global project kuryr
%global service kuryr-kubernetes
%global service_py kuryr_kubernetes

Name: openstack-%service
Version: XXX
Release: XXX
Epoch: 0
Summary: OpenStack networking integration with Kubernetes

License:    ASL 2.0
URL:        http://docs.openstack.org/developer/kuryr-kubernetes/

Source0:    https://tarballs.openstack.org/%{project}/%{service}-%{upstream_version}.tar.gz
Source1:    kuryr.logrotate
Source2:    kuryr-controller.service

BuildArch: noarch

# debtcollector is a hidden dependency of oslo-config
BuildRequires:  git
BuildRequires:  python2-devel
BuildRequires:  python-d2to1
BuildRequires:  python-oslo-config
BuildRequires:  python2-debtcollector
BuildRequires:  python-pbr
BuildRequires:  python-setuptools
BuildRequires:  systemd-units

Requires:       python-%{service} = %{version}-%{release}

Requires(pre): shadow-utils
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
Kuryr-Kubernetes brings OpenStack networking to Kubernetes clusters

%package -n python-%{service}
Summary:        Kuryr Kubernetes libraries
Requires:       python2-%{project}-lib >= 0.4.0
Requires:       python-pbr >= 1.8
Requires:       python2-pyroute2 >= 0.4.13
Requires:       python-requests >= 2.10.0
Requires:       python-eventlet >= 0.18.2
Requires:       python-oslo-config >= 2:3.14.0
Requires:       python-oslo-log >= 3.11.0
Requires:       python-oslo-serialization >= 1.10.0
Requires:       python-oslo-service >= 1.10.0
Requires:       python-oslo-utils >= 3.18.0
Requires:       python-os-vif >= 1.4.0
Requires:       python-six >= 1.9.0
Requires:       python-stevedore >= 1.17.1

%description -n python-%{service}
Kuryr Kubernetes provides a Controller that watches the Kubernetes API for
Object changes and manages Neutron resources to provide the Kubernetes Cluster
with OpensStack networking.

This package contains the Kuryr Kubernetes Python library.

%package -n python-%{service}-tests
Summary:        Kuryr Kubernetes tests
Requires:       python-%{service} = %{epoch}:%{version}-%{release}
Requires:       python-mock >= 2.0
Requires:       python-oslotest >= 1.10.0
Requires:       python-testrepository >= 0.0.18
Requires:       python-testscenarios >= 0.4
Requires:       python-testtools >= 1.4.0

%description -n python-%{service}-tests
Kuryr Kubernetes provides a Controller that watches the Kubernetes API for
Object changes and manages Neutron resources to provide the Kubernetes Cluster
with OpensStack networking.

This package contains the Kuryr Kubernetes tests.

%package common
Summary:        Kuryr Kubernetes common files
Group:          Applications/System
Requires:   python-%{service} = %{epoch}:%{version}-%{release}

%description common
%{common_desc}

This package contains Kuryr files common to all services.

%package controller
Summary: Kuryr Kubernetes Controller
Requires: openstack-%{service}-common = %{epoch}:%{version}-%{release}

%description controller
Kuryr Kubernetes provides a Controller that watches the Kubernetes API for
Object changes and manages Neutron resources to provide the Kubernetes Cluster
with OpensStack networking.

This package contains the Kuryr Kubernetes Controller that watches the
Kubernetes API and adds metadata to its Objects about the OpenStack resources
it obtains.

%package cni
Summary: CNI plugin
Requires: openstack-%{service}-common = %{epoch}:%{version}-%{release}

%description cni
Kuryr Kubernetes provides a Controller that watches the Kubernetes API for
Object changes and manages Neutron resources to provide the Kubernetes Cluster
with OpensStack networking.

This package contains the Kuryr Kubernetes Container Network Interface driver
that Kubelet calls to.

%prep
%autosetup -n %{service}-%{upstream_version} -S git

find %{service_py} -name \*.py -exec sed -i '/\/usr\/bin\/env python/{d;q}' {} +

# Let's handle dependencies ourseleves
rm -f requirements.txt

# Kill egg-info in order to generate new SOURCES.txt
rm -rf kuryr_kubernetes.egg-info

%build
export SKIP_PIP_INSTALL=1
%{__python2} setup.py build

# Generate configuration files
PYTHONPATH=. tools/generate_config_file_samples.sh
find etc -name *.sample | while read filename
do
    filedir=$(dirname $filename)
    file=$(basename $filename .sample)
    mv ${filename} ${filedir}/${file}
done

%install
%{__python2} setup.py install -O1 --skip-build --root=%{buildroot}

# Remove unused files
rm -rf %{buildroot}%{python2_sitelib}/bin
rm -rf %{buildroot}%{python2_sitelib}/doc
rm -rf %{buildroot}%{python2_sitelib}/tools

# Move config files to proper location
install -d -m 755 %{buildroot}%{_sysconfdir}/%{project}
install -d -m 755 %{buildroot}%{_localstatedir}/log/%{project}
mv etc/kuryr.conf %{buildroot}%{_sysconfdir}/%{project}/kuryr.conf

# Install logrotate
install -p -D -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/openstack-%{service}

# Install systemd units
install -p -D -m 644 %{SOURCE2} %{buildroot}%{_unitdir}/kuryr-controller.service
# TODO(apuimedo): systemd tempfiles to setup /run/kuryr

%pre -n python-%{service}
getent group %{project} >/dev/null || groupadd -r %{project}
getent passwd %{project} >/dev/null || \
    useradd -r -g %{project} -d %{_sharedstatedir}/%{project} -s /sbin/nologin \
    -c "OpenStack Kuryr Daemons" %{service}
exit 0

%post controller
%systemd_post kuryr-controller.service

%preun controller
%systemd_preun kuryr-controller.service

%postun controller
%systemd_postun_with_restart kuryr-controller.service

%files controller
%license LICENSE
%{_bindir}/kuryr-k8s-controller
%{_unitdir}/kuryr-controller.service
%{python2_sitelib}/%{service_py}/controller

%files -n python-%{service}-tests
%license LICENSE
%{python2_sitelib}/%{service_py}/tests

%files -n python-%{service}
%license LICENSE
%{python2_sitelib}/%{service_py}
%{python2_sitelib}/%{service_py}-*.egg-info
%exclude %{python2_sitelib}/%{service_py}/tests
%exclude %{python2_sitelib}/%{service_py}/cni
%exclude %{python2_sitelib}/%{service_py}/controller

%files common
%license LICENSE
%doc README.rst
%config(noreplace) %attr(0640, root, %{project}) %{_sysconfdir}/%{project}/%{project}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/*
%dir %attr(0750, %{project}, %{project}) %{_localstatedir}/log/%{project}

%files cni
%license LICENSE
%{_bindir}/kuryr-cni
%{python2_sitelib}/%{service_py}/cni
%{python2_sitelib}/%{service_py}/cmd/cni.py*

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
